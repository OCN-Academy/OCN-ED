%*******************************************************************************
%   OCN-SIM Flex User manual
%
%   Copyright (C) 2017 by Christoph Otto
%   christoph@ocnacademy.org
%   www.ocnacademy.org
%
%   MODULE :: Fact sheets layout in corporate design
%   ___   ____ _   _    _____ ____  
%  / _ \ / ___| \ | |  | ____|  _ \ 
% | | | | |   |  \| |  |  _| | | | |
% | |_| | |___| |\  |  | |___| |_| |
%  \___/ \____|_| \_|  |_____|____/ 
%
%   LICENSE
%   CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by/4.0/legalcode.txt)
%
%   This file is part of the project OCN-ED.
% 
%*******************************************************************************

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{ragged2e}

\usepackage{tabularx}
\usepackage{colortbl}

\usepackage{listings}
\usepackage{lstautogobble}

% Custom colors
\usepackage{ocncolors}


%==============================================================================
%   FORMAT
%==============================================================================
% --- Widths ---
% Separation between columns
\newlength{\sepwid}
\setlength{\sepwid}      {0.05\paperwidth}

% Used to calc geometry in markboxes. Compare BOXED BLOCK FOR SPECIAL SECTIONS below.
\newlength{\origArrayrulewidth}
\newlength{\headwidth}

% Spacing
\setlength{\columnsep}{\sepwid}
\setlength{\parskip}{1em}

% --- Page geometry ---
\usepackage[
    a4paper,
    landscape,
    top=16mm,
    bottom=20mm,
    inner=1.2\sepwid,
    outer=1.2\sepwid,
    headsep=3mm
]{geometry}


% --- Koma script ---
\usepackage{scrlayer-scrpage}
\pagestyle{scrheadings}


\setkomafont{pagehead}{\normalfont\color{tSBII}}
\setkomafont{pagefoot}{\normalfont\color{tSBII}}

\setheadsepline{0.05cm}
\setfootsepline{0.05cm}

\ihead{\raisebox{-1.em}{\Huge\sffamily\textbf{\subsecname}}}
\ohead{\raisebox{0.5mm}{\includegraphics[height=8mm]{pic/Logo.pdf}}}

\ifoot{\vskip-0.8em \color{black}\normalsize \OCNHeadingInBlock{\secname} ~|~ \theauthor ~|~ \theemail}
\cfoot{}
\ofoot{\vskip-0.8em \color{black}\pagemark}

\setkomafont{captionlabel}{\bfseries\sffamily\color{tSBII}}



% --- Others ---
% Captions
\usepackage{caption}
\DeclareCaptionLabelSeparator{mediumdist}{\hspace{0.8em}}
\captionsetup{labelsep=mediumdist,hangindent=-10mm}

% Triangular itemeize bullets
\renewcommand{\labelitemi}{\raisebox{0.25em}{\tiny$\color{tSBII}\blacktriangleright$}}

\bibliographystyle{plain}

% Requirement: Lato font for latex. On Ubuntu, it's e.g. contained in texlive-fonts-extra.
% In a terminal, do: sudo apt-get install texlive-fonts-extra.
% If you don't want to install the lato font for latex, just comment the line below.
% However, you're no longer corporate-identity-compliant then.
% Set sans serif font to lato in accordance with logo. hardest task was to figure
% out the font's name. compare:
% http://tex.stackexchange.com/questions/25249/how-do-i-use-a-particular-font-for-a-small-section-of-text-in-my-document/25251
\renewcommand{\sfdefault}{fla}


%==============================================================================
% CUSTOM COMMANDS
%==============================================================================
\lstdefinestyle{OCNUnformattedCode}{
    backgroundcolor=\color{OCNBackgroundCode},
    basicstyle=\ttfamily,
    autogobble=true,
    frame=single,
    framesep  = 1mm,
    framerule = 0.1mm,
    xleftmargin=1.1mm,
    xrightmargin=1.1mm
}

\lstdefinestyle{OCNInputFile}{
    language=Python,
    backgroundcolor=\color{OCNBackgroundCode},
    keywordstyle=\color{tSBII}\bfseries,
    commentstyle=\color{tCIII},
    basicstyle=\ttfamily,
    autogobble=true,
    showstringspaces=false,
    frame=single,
    framesep  = 1mm,
    framerule = 0.1mm,
    xleftmargin=1.1mm,
    xrightmargin=1.1mm
}


%==============================================================================
% HEADINGS
%==============================================================================
\newcommand{\OCNHeadingInBlock}[1]{\textsf{\textbf{#1}}}
\newcommand{\OCNHeadBl}[1]{{\color{tSBII}\textsf{\textbf{#1}}}}
\newcommand{\OCNHeadOr}[1]{{\color{ubOr}\textsf{\textbf{#1}}}}


%==============================================================================
% SECTIONING
%==============================================================================
% --- Sectioning ---
\newcommand{\secname}{}
\newcommand{\subsecname}{}
\renewcommand{\section}[1]{\clearpage\renewcommand{\secname}{#1} \phantomsection \addcontentsline{toc}{section}{#1}}
\renewcommand{\subsection}[1]{\clearpage\renewcommand{\subsecname}{#1} \phantomsection \addcontentsline{toc}{subsection}{#1}}


%==============================================================================
% SUBSUBSECTION
%   Patching subsubsection to include coloured box
%   compare e.g. http://www.tex.ac.uk/FAQ-patch.html
%==============================================================================
\let\OldSubsubsection\subsubsection
\renewcommand{\subsubsection}[1]{%
    %\OldSubsubsection{#1}
	{\setlength{\fboxsep}{0pt}
	\colorbox{ubOr}{%
		\begin{minipage}[c][3.5ex][c]{\columnwidth}
        	\centering
        	{\color{white}\OCNHeadingInBlock{#1}}
		\end{minipage}}}
	\justifying
    	
}


%==============================================================================
% TABLE
%==============================================================================
\newenvironment{OCNTable}[1]{%
    \setlength{\origArrayrulewidth}{\arrayrulewidth}%
    \setlength{\arrayrulewidth}{0.5pt}%
    %
    \tabularx{\linewidth}{|*{#1}{l}X|}
        \hline%
        \rowcolor{OCNBackgroundCode}%
}{%
    \endtabularx%
    \setlength{\arrayrulewidth}{\origArrayrulewidth}%
}


%==============================================================================
% BOXED BLOCK FOR SPECIAL SECTIONS
%	Purpose of this is to start moving away from beamer elements so as to
%   increase portability. However, this fails when using framed verbatim listings.
%   Still keeping this, as it was promising otherwise and so I'm just not using
%   the framed verbatims any longer.
%==============================================================================
\newenvironment{OCNMarkBox}[1]{%
    
    \setlength{\origArrayrulewidth}{\arrayrulewidth}
    \setlength{\arrayrulewidth}{0.5mm}
    \arrayrulecolor{tSBII}
    
    \setlength{\headwidth}{\columnwidth}
    \addtolength{\headwidth}{-2\tabcolsep}
    \addtolength{\headwidth}{-2\arrayrulewidth}
    
    \tabular{|p{\headwidth}|}
    \rowcolor{tSBII}
    \begin{minipage}[c][3.5ex][c]{\headwidth}
    	\centering
    	{\color{white}\OCNHeadingInBlock{#1}}
    \end{minipage}
    \\ \\
}{%
    \\ \hline
    \endtabular
    \setlength{\arrayrulewidth}{\origArrayrulewidth}
    \arrayrulecolor{black}
}
