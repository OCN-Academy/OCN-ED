%*******************************************************************************
%   OCN-SIM Flex User manual
%
%   Copyright (C) 2017 by Christoph Otto
%   christoph@ocnacademy.org
%   www.ocnacademy.org
%
%   MODULE :: List of symbols
%   ___   ____ _   _    _____ ____  
%  / _ \ / ___| \ | |  | ____|  _ \ 
% | | | | |   |  \| |  |  _| | | | |
% | |_| | |___| |\  |  | |___| |_| |
%  \___/ \____|_| \_|  |_____|____/ 
%
%   LICENSE
%   CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by/4.0/legalcode.txt)
%
%   This file is part of the project OCN-ED.
% 
%*******************************************************************************

\usepackage{glossaries}
\usepackage{ocnmath}

\newglossaryentry{ic}
{
  name={\ensuremath{i_{\mathrm{C}}}},
  description= {Counting index for implicit constraint equations},
  sort=ic
}

\newglossaryentry{gic}
{
  name={\ensuremath{\bar{g}_{i_{\mathrm{C}}}}},
  description={Implicit constraint $i$},
  sort=gic
}

\newglossaryentry{kOne}{
  name={\ensuremath{k1\left(i_\mathrm{C}\right)}},
  description={Node number $1$ in distance constraint $\bar{g}_{i_{\mathrm{C}}}$},
  sort=k1
}

\newglossaryentry{kTwo}{
  name={\ensuremath{k2\left(i_\mathrm{C}\right)}},
  description={Node number $2$ in distance constraint $\bar{g}_{i_{\mathrm{C}}}$},
  sort=k2
}

\newglossaryentry{rGIC}{
  name={\ensuremath{\bm{r}_{\mathrm{g},i_\mathrm{C}}}},
  description={Difference vector of the positions of the two nodes pointing from \gls{kOne} to \gls{kTwo}},
  sort=rgic
}

\newglossaryentry{uGIC}{
  name={\ensuremath{\bm{u}_{\mathrm{g},i_{\mathrm{C}}}}},
  description={Direction of constraint $i_{\mathrm{C}}$, i.e. the unit vector pointing from \gls{kOne} to \gls{kTwo}},
  sort=ugic
}

\newglossaryentry{n_nodes}
{
  name={\ensuremath{n_{\mathrm{N}}}},
  description={Total number of nodes in the system},
  sort=nn
}

\newglossaryentry{r_sys}
{
  name={\ensuremath{\bm{r}}},
  description={Position vector of all nodes in coordinate system $\mathcal{K}_0$},
  sort=r
}

\newglossaryentry{r_i}
{
  name={\ensuremath{\bm{r}_i}},
  description={Position vector of node $i$ in coordinate system $\mathcal{K}_0$},
  sort=ri
}

\newglossaryentry{v_sys}
{
  name={\ensuremath{\bm{v}}},
  description={Velocity vector of the complete system of nodes in coordinate system $\mathcal{K}_0$},
  sort=v
}

\newglossaryentry{v_i}
{
  name={\ensuremath{\bm{v}_i}},
  description={Velocity vector of node $i$ in coordinate system $\mathcal{K}_0$},
  sort=vi
}

\newglossaryentry{a_sys}
{
  name={\ensuremath{\bm{a}}},
  description={Acceleration vector of the complete system of nodes},
  sort=a
}

\newglossaryentry{a_i}
{
  name={\ensuremath{\bm{a}_i}},
  description={Acceleration vector of node $i$ in coordinate system $\mathcal{K}_0$},
  sort=ai
}

\newglossaryentry{fe_sys}
{
    name={\ensuremath{\bm{f}^{\mathrm{e}}}},
    description={Vector of external forces acting on all nodes},
    sort=fe
    }

\newglossaryentry{fe_q_sys}
{
    name={\ensuremath{\bm{f}^{\mathrm{q,e}}}},
    description={Vector of generalised external forces acting on all nodes in terms of minimal coordinates \gls{q_sys}},
    sort=feq
}

\newglossaryentry{fr_sys}
{
    name={\ensuremath{\bm{f}^{\mathrm{r}}}},
    description={Vector of reaction forces acting on all nodes},
    sort=fr
}

\newglossaryentry{fr_i}
{
    name={\ensuremath{\bm{f}^{\mathrm{r}}_i}},
    description={Vector of reaction forces acting on nodes $i$},
    sort=fri
}

\newglossaryentry{fbr_i}
{
    name={\ensuremath{\bm{f}^{\mathrm{b}}_i}},
    description={Bar reaction force vector acting along bar element $i$},
    sort=fbri
}

\newglossaryentry{n_q}
{
    name={\ensuremath{n_\mathrm{q}}},
    description={Total number of minimal coordinates},
    sort=nq
}

\newglossaryentry{q_sys}
{
  name={\ensuremath{\bm{q}}},
  description={System vector of joint coordinates},
  sort=q
}

\newglossaryentry{q_dot_sys}
{
  name={\ensuremath{\dot{\bm{q}}}},
  description={System vector of joint velocities},
  sort=qdot
}

\newglossaryentry{q_ddot_sys}
{
    name={\ensuremath{\ddot{\bm{q}}}},
    description={System vector of joint accelerations},
    sort=qddot
}


\newglossaryentry{JAC}
{
  name={\ensuremath{\bm{J}}},
  description={\textsc{Jacobian}-matrix of the complete system of nodes},
  sort=JAC
}

\newglossaryentry{JAC_ij}
{
  name={\ensuremath{\bm{J}_{ij}}},
  description={\textsc{Jacobian}-matrix of node $i$ with respect to joint $j$},
  sort=JACij
}

\newglossaryentry{P_i}
{
  name={\ensuremath{\mathbb{P}_i}},
  description={Set of predecessor nodes of node $i$},
  sort=Pi
}

\newglossaryentry{P_i_bar}
{
  name={\ensuremath{\bar{\mathbb{P}}_i}},
  description={Set of predecessor nodes of node $i$ extended by node $i$ itself},
  sort=Pib
}


\newglossaryentry{M_Sys}
{
    name={\ensuremath{\bm{M}}},
    description={Mass matrix of the complete system of nodes},
    sort=M
}

\newglossaryentry{M_q_Sys}
{
    name={\ensuremath{\bm{M}^{\mathrm{q}}}},
    description={Generalised mass matrix of the complete system of nodes in terms of minimal coordinates \gls{q_sys}},
    sort=Mq
}

\newglossaryentry{M_i}
{
    name={\ensuremath{\bm{M}_i}},
    description={Mass matrix of node i},
    sort=Mi
}



\makeglossaries